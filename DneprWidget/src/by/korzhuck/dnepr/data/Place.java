package by.korzhuck.dnepr.data;

import by.korzhuck.dnepr.DneprApplication;
import by.korzhuck.dnepr.R;

public class Place {
   public Place(String stadium, String city, String country) {
      _stadium = stadium;
      _city = city;
      _country = country;
   }

   @Override
   public String toString() {
      return DneprApplication.context().getString(R.string.title_stadium) + " " + _stadium + ", " + _city;
   }

   private final String _stadium;
   private final String _city;
   private final String _country;
}
