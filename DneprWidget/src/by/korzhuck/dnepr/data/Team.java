package by.korzhuck.dnepr.data;

public class Team {
   public Team(long id, String name, String logo) {
      _id = id;
      _name = name;
      _logo = logo;
   }

   @Override
   public String toString() {
      return _name;
   }

   private final long _id;
   private final String _name;
   private final String _logo;
}
