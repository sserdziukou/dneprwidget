package by.korzhuck.dnepr.data;

import java.text.DateFormat;
import java.util.Date;

public class Match {
   public Match(Date date, Place place, Team homeTeam, Team awayTeam) {
      _date = date;
      _place = place;
      _homeTeam = homeTeam;
      _awayTeam = awayTeam;
   }

   @Override
   public String toString() {
      return _dateFormat.format(_date) + " " + _place.toString() + "\n" + _homeTeam.toString() + " - " + _awayTeam.toString();
   }

   private final DateFormat _dateFormat = DateFormat.getDateInstance(DateFormat.SHORT);
   private final Date _date;
   private final Place _place;
   private final Team _homeTeam;
   private final Team _awayTeam;

   public Date getDate() {
      return _date;
   }
}
