package by.korzhuck.dnepr.data;

public class Standing {
   public Standing(Team team) {
      _team = team;
   }

   @Override
   public String toString() {
      return _team.toString();
   }

   private Team _team;
}
