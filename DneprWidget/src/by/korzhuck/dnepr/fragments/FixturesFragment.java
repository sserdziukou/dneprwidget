package by.korzhuck.dnepr.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import by.korzhuck.dnepr.DneprApplication;
import by.korzhuck.dnepr.R;
import by.korzhuck.dnepr.adapters.FixturesAdapter;
import by.korzhuck.dnepr.data.Match;
import by.korzhuck.dnepr.functional.Receiver;
import by.korzhuck.dnepr.functional.TabFragmentFactory;
import by.korzhuck.dnepr.providers.FixturesProvider;

import java.util.List;

public class FixturesFragment extends Fragment {
   public static TabFragmentFactory factory() {
      return new TabFragmentFactory() {
         @Override
         public Fragment create() {
            return new FixturesFragment();
         }

         @Override
         public String title() {
            return DneprApplication.context().getString(R.string.title_fixtures);
         }
      };
   }

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      View view = inflater.inflate(R.layout.fragment_fixtures, null);
      _list = (ListView) view.findViewById(R.id.list);
      return view;
   }

   @Override
   public void onViewCreated(View view, Bundle savedInstanceState) {
      super.onViewCreated(view, savedInstanceState);
      _fixturesProvider.getAll(new Receiver<List<Match>>() {
         @Override
         public void receive(List<Match> data) {
            if (isVisible()) {
               _list.setAdapter(new FixturesAdapter(getActivity(), data));
            }
         }
      });
   }

   private final FixturesProvider _fixturesProvider = new FixturesProvider();
   private ListView _list;
}
