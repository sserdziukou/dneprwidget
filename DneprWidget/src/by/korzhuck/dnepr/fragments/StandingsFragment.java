package by.korzhuck.dnepr.fragments;

import android.os.Bundle;
import android.support.v4.app.Fragment;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ListView;
import by.korzhuck.dnepr.DneprApplication;
import by.korzhuck.dnepr.R;
import by.korzhuck.dnepr.adapters.StandingsAdapter;
import by.korzhuck.dnepr.data.Standing;
import by.korzhuck.dnepr.functional.Receiver;
import by.korzhuck.dnepr.functional.TabFragmentFactory;
import by.korzhuck.dnepr.providers.StandingsProvider;

import java.util.List;

public class StandingsFragment extends Fragment {
   public static TabFragmentFactory factory() {
      return new TabFragmentFactory() {
         @Override
         public Fragment create() {
            return new StandingsFragment();
         }

         @Override
         public String title() {
            return DneprApplication.context().getString(R.string.title_standings);
         }
      };
   }

   @Override
   public View onCreateView(LayoutInflater inflater, ViewGroup container, Bundle savedInstanceState) {
      View view = inflater.inflate(R.layout.fragment_standings, null);
      _list = (ListView) view.findViewById(R.id.list);
      return view;
   }

   @Override
   public void onViewCreated(View view, Bundle savedInstanceState) {
      super.onViewCreated(view, savedInstanceState);
      _standingsProvider.getAll(new Receiver<List<Standing>>() {
         @Override
         public void receive(List<Standing> data) {
            if (isVisible()) {
               _list.setAdapter(new StandingsAdapter(getActivity(), data));
            }
         }
      });
   }

   private final StandingsProvider _standingsProvider = new StandingsProvider();
   private ListView _list;
}
