package by.korzhuck.dnepr.providers;

import android.content.res.AssetManager;
import by.korzhuck.dnepr.DneprApplication;
import by.korzhuck.dnepr.data.Match;
import by.korzhuck.dnepr.functional.Maybe;
import by.korzhuck.dnepr.functional.Receiver;
import by.korzhuck.dnepr.xml.FixturesParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.Date;
import java.util.List;

public class FixturesProvider {
   public void getAll(Receiver<List<Match>> resultReceiver) {
      try {
         _parser.parse(_assetManager.open("fixtures-ru.xml"), resultReceiver);
      } catch (XmlPullParserException | IOException e) {
         e.printStackTrace();
      }
   }

   private FixturesParser _parser = new FixturesParser();
   private AssetManager _assetManager = DneprApplication.context().getAssets();

   public void getNextMatch(final Receiver<Maybe<Match>> receiver) {
      getAll(new Receiver<List<Match>>() {
         @Override
         public void receive(List<Match> data) {
            receiver.receive(findNext(data));
         }
      });
   }

   private Maybe<Match> findNext(List<Match> data) {
      for (Match match : data) {
         if (match.getDate().getTime() > new Date().getTime()) {
            return Maybe.value(match);
         }
      }
      return Maybe.nothing();
   }
}
