package by.korzhuck.dnepr.providers;

import android.content.res.AssetManager;
import by.korzhuck.dnepr.DneprApplication;
import by.korzhuck.dnepr.data.Team;
import by.korzhuck.dnepr.functional.Receiver;
import by.korzhuck.dnepr.xml.TeamsParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.util.List;

public class TeamsProvider {
   public void getTeams(Receiver<List<Team>> receiver) {
      try {
         _parser.parse(_assetManager.open("teams-ru.xml"), receiver);
      } catch (XmlPullParserException | IOException e) {
         e.printStackTrace();
      }
   }

   private TeamsParser _parser = new TeamsParser();
   private AssetManager _assetManager = DneprApplication.context().getAssets();
}
