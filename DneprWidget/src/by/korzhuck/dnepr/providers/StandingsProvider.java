package by.korzhuck.dnepr.providers;

import by.korzhuck.dnepr.data.Standing;
import by.korzhuck.dnepr.data.Team;
import by.korzhuck.dnepr.functional.Receiver;

import java.util.LinkedList;
import java.util.List;

public class StandingsProvider {
   public void getAll(final Receiver<List<Standing>> resultReceiver) {
      _teamsProvider.getTeams(new Receiver<List<Team>>() {
         @Override
         public void receive(List<Team> data) {
            resultReceiver.receive(toStandings(data));
         }
      });
   }

   private List<Standing> toStandings(List<Team> teams) {
      List<Standing> standings = new LinkedList<>();

      for (Team team : teams) {
         standings.add(new Standing(team));
      }

      return standings;
   }

   private TeamsProvider _teamsProvider = new TeamsProvider();
}
