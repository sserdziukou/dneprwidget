package by.korzhuck.dnepr;

import android.app.Activity;
import android.os.Bundle;

public class UpcomingWidgetConfigureActivity extends Activity {
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setResult(RESULT_OK, getIntent());
      finish();
   }
}
