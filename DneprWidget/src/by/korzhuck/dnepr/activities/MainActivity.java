package by.korzhuck.dnepr.activities;

import android.os.Bundle;
import android.support.v4.app.FragmentActivity;
import android.support.v4.view.ViewPager;
import by.korzhuck.dnepr.R;
import by.korzhuck.dnepr.helpers.FragmentsHelper;

public class MainActivity extends FragmentActivity {
   @Override
   protected void onCreate(Bundle savedInstanceState) {
      super.onCreate(savedInstanceState);
      setContentView(R.layout.activity_main);

      ((ViewPager) findViewById(R.id.pager)).setAdapter(_fragmentsHelper.createPagerAdapter(getSupportFragmentManager()));
   }

   private FragmentsHelper _fragmentsHelper = new FragmentsHelper();
}
