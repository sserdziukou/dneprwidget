package by.korzhuck.dnepr.helpers;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;
import android.support.v4.view.PagerAdapter;
import by.korzhuck.dnepr.fragments.FixturesFragment;
import by.korzhuck.dnepr.fragments.StandingsFragment;
import by.korzhuck.dnepr.functional.TabFragmentFactory;

import java.util.Arrays;
import java.util.List;

public class FragmentsHelper {

   public PagerAdapter createPagerAdapter(FragmentManager fragmentManager) {
      return new FragmentPagerAdapter(fragmentManager) {
         @Override
         public Fragment getItem(int position) {
            return createFragment(position);
         }

         @Override
         public int getCount() {
            return _factories.size();
         }

         @Override
         public CharSequence getPageTitle(int position) {
            return getFactory(position).title();
         }
      };
   }

   private Fragment createFragment(int position) {
      return getFactory(position).create();
   }

   private TabFragmentFactory getFactory(int position) {
      return _factories.get(position);
   }

   private List<TabFragmentFactory> _factories = Arrays.asList(StandingsFragment.factory(),
                                                               FixturesFragment.factory());
}
