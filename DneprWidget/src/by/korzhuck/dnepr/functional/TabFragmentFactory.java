package by.korzhuck.dnepr.functional;

import android.support.v4.app.Fragment;

public interface TabFragmentFactory extends Factory<Fragment> {
   public String title();
}
