package by.korzhuck.dnepr.functional;

public interface Receiver<Type> {
   void receive(Type data);
}
