package by.korzhuck.dnepr.functional;

public interface Factory<Data> {
   Data create();
}
