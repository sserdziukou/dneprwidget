package by.korzhuck.dnepr.functional;

public final class Maybe<T> {
   public static <T> Maybe<T> value(T value) {
      return new Maybe<>(value);
   }

   public static <T> Maybe<T> nothing() {
      return new Maybe<>();
   }

   public static <T> Maybe<T> nullsAreNothing(T value) {
      if (value != null) {
         return new Maybe<>(value);
      }

      return new Maybe<>();
   }

   private Maybe(T value) {
      _value = value;
      _haveValue = true;
   }

   private Maybe() {
      _value = null;
      _haveValue = false;
   }

   public T value() {
      if (!isValue()) {
         throw new Error("Accessing value from maybe without value");
      }

      return _value;
   }

   public boolean isValue() {
      return _haveValue;
   }

   public T or(T defaultValue) {
      if (_haveValue) {
         return _value;
      }

      return defaultValue;
   }

   private T _value;
   private boolean _haveValue;
}
