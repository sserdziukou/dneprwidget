package by.korzhuck.dnepr.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.widget.RemoteViews;
import by.korzhuck.dnepr.R;
import by.korzhuck.dnepr.data.Match;
import by.korzhuck.dnepr.functional.Maybe;
import by.korzhuck.dnepr.functional.Receiver;
import by.korzhuck.dnepr.providers.FixturesProvider;

public class UpcomingWidget extends DneprWidget {
   private FixturesProvider _fixturesProvider = new FixturesProvider();

   @Override
   public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
      for (int appWidgetId : appWidgetIds) {
         final RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_upcoming);
         configureMainClick(context, views, R.id.parent);
         _fixturesProvider.getNextMatch(new Receiver<Maybe<Match>>() {
            @Override
            public void receive(Maybe<Match> data) {
               if (data.isValue()) {
                  views.setTextViewText(R.id.match, data.value().toString());
               }
            }
         });

         appWidgetManager.updateAppWidget(appWidgetId, views);
      }
   }
}
