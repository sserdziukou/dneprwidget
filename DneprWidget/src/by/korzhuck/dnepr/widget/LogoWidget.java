package by.korzhuck.dnepr.widget;

import android.appwidget.AppWidgetManager;
import android.content.Context;
import android.widget.RemoteViews;
import by.korzhuck.dnepr.R;

public class LogoWidget extends DneprWidget {
   @Override
   public void onUpdate(Context context, AppWidgetManager appWidgetManager, int[] appWidgetIds) {
      for (int appWidgetId : appWidgetIds) {
         RemoteViews views = new RemoteViews(context.getPackageName(), R.layout.widget_logo);
         configureMainClick(context, views, R.id.logo);
         appWidgetManager.updateAppWidget(appWidgetId, views);
      }
   }
}
