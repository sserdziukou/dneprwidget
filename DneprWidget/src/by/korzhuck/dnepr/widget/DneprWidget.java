package by.korzhuck.dnepr.widget;

import android.app.PendingIntent;
import android.appwidget.AppWidgetProvider;
import android.content.Context;
import android.content.Intent;
import android.widget.RemoteViews;
import by.korzhuck.dnepr.activities.MainActivity;

public class DneprWidget extends AppWidgetProvider {
   protected void configureMainClick(Context context, RemoteViews views, int clickableId) {
      Intent intent = new Intent(context, MainActivity.class);
      PendingIntent pendingIntent = PendingIntent.getActivity(context, 0, intent, 0);
      views.setOnClickPendingIntent(clickableId, pendingIntent);
   }
}
