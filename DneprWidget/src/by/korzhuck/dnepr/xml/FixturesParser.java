package by.korzhuck.dnepr.xml;

import android.util.Xml;
import by.korzhuck.dnepr.data.Match;
import by.korzhuck.dnepr.data.Place;
import by.korzhuck.dnepr.data.Team;
import by.korzhuck.dnepr.functional.Receiver;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.Date;
import java.util.LinkedList;
import java.util.List;

public class FixturesParser extends XmlParser {
   public void parse(InputStream input, Receiver<List<Match>> resultReceiver) throws XmlPullParserException, IOException {
      try {
         XmlPullParser parser = Xml.newPullParser();
         parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
         parser.setInput(input, null);
         parser.nextTag();
         resultReceiver.receive(readFixtures(parser));
      } finally {
         input.close();
      }
   }

   private List<Match> readFixtures(XmlPullParser parser) throws XmlPullParserException, IOException {
      List<Match> fixtures = new LinkedList<>();

      parser.require(XmlPullParser.START_TAG, DEFAULT_NAMESPACE, "fixtures");
      while (parser.next() != XmlPullParser.END_TAG) {
         if (parser.getEventType() != XmlPullParser.START_TAG) {
            continue;
         }
         String name = parser.getName();
         if (name.equals("match")) {
            fixtures.add(readMatch(parser));
         } else {
            skip(parser);
         }
      }
      return fixtures;
   }

   private Match readMatch(XmlPullParser parser) throws XmlPullParserException, IOException {
      parser.require(XmlPullParser.START_TAG, DEFAULT_NAMESPACE, "match");
      long time = 0;
      Place place = null;
      Team homeTeam = null;
      Team awayTeam = null;
      while (parser.next() != XmlPullParser.END_TAG) {
         if (parser.getEventType() != XmlPullParser.START_TAG) {
            continue;
         }
         String tagName = parser.getName();
         if (tagName.equals("date")) {
            time = readLong("date", parser);
         } else if (tagName.equals("place")) {
            place = readPlace(parser);
         } else if (tagName.equals("home-team")) {
            homeTeam = readTeam("home-team", parser);
         } else if (tagName.equals("away-team")) {
            awayTeam = readTeam("away-team", parser);
         } else {
            skip(parser);
         }
      }

      return new Match(new Date(time), place, homeTeam, awayTeam);
   }

   private Place readPlace(XmlPullParser parser) throws XmlPullParserException, IOException {
      parser.require(XmlPullParser.START_TAG, DEFAULT_NAMESPACE, "place");
      String stadium = null;
      String city = null;
      String country = null;
      while (parser.next() != XmlPullParser.END_TAG) {
         if (parser.getEventType() != XmlPullParser.START_TAG) {
            continue;
         }
         String tagName = parser.getName();
         if (tagName.equals("stadium")) {
            stadium = readString("stadium", parser);
         } else if (tagName.equals("city")) {
            city = readString("city", parser);
         } else if (tagName.equals("country")) {
            country = readString("country", parser);
         } else {
            skip(parser);
         }
      }
      return new Place(stadium, city, country);
   }

   private Team readTeam(String tag, XmlPullParser parser) throws XmlPullParserException, IOException {
      parser.require(XmlPullParser.START_TAG, DEFAULT_NAMESPACE, tag);
      long id = 0;
      String name = null;
      String logo = null;
      while (parser.next() != XmlPullParser.END_TAG) {
         if (parser.getEventType() != XmlPullParser.START_TAG) {
            continue;
         }
         String tagName = parser.getName();
         if (tagName.equals("id")) {
            id = readLong("id", parser);
         } else if (tagName.equals("name")) {
            name = readString("name", parser);
         } else if (tagName.equals("logo")) {
            logo = readString("logo", parser);
         } else {
            skip(parser);
         }
      }
      return new Team(id, name, logo);
   }
}
