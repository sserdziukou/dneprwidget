package by.korzhuck.dnepr.xml;

import android.util.Xml;
import by.korzhuck.dnepr.data.Team;
import by.korzhuck.dnepr.functional.Receiver;
import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;
import java.io.InputStream;
import java.util.LinkedList;
import java.util.List;

public class TeamsParser extends XmlParser {
   public void parse(InputStream input, Receiver<List<Team>> resultReceiver) throws XmlPullParserException, IOException {
      try {
         XmlPullParser parser = Xml.newPullParser();
         parser.setFeature(XmlPullParser.FEATURE_PROCESS_NAMESPACES, false);
         parser.setInput(input, null);
         parser.nextTag();
         resultReceiver.receive(readTeams(parser));
      } finally {
         input.close();
      }
   }

   private List<Team> readTeams(XmlPullParser parser) throws XmlPullParserException, IOException {
      List<Team> entries = new LinkedList<>();

      parser.require(XmlPullParser.START_TAG, DEFAULT_NAMESPACE, "teams");
      while (parser.next() != XmlPullParser.END_TAG) {
         if (parser.getEventType() != XmlPullParser.START_TAG) {
            continue;
         }
         String name = parser.getName();
         if (name.equals("team")) {
            entries.add(readTeam(parser));
         } else {
            skip(parser);
         }
      }
      return entries;
   }

   private Team readTeam(XmlPullParser parser) throws XmlPullParserException, IOException {
      parser.require(XmlPullParser.START_TAG, DEFAULT_NAMESPACE, "team");
      long id = 0;
      String name = null;
      String logo = null;
      while (parser.next() != XmlPullParser.END_TAG) {
         if (parser.getEventType() != XmlPullParser.START_TAG) {
            continue;
         }
         String tagName = parser.getName();
         if (tagName.equals("id")) {
            id = readLong("id", parser);
         } else if (tagName.equals("name")) {
            name = readString("name", parser);
         } else if (tagName.equals("logo")) {
            logo = readString("logo", parser);
         } else {
            skip(parser);
         }
      }
      return new Team(id, name, logo);
   }

}
