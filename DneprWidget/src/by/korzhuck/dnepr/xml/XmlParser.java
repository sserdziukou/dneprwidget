package by.korzhuck.dnepr.xml;

import org.xmlpull.v1.XmlPullParser;
import org.xmlpull.v1.XmlPullParserException;

import java.io.IOException;

public class XmlParser {
   protected static final String DEFAULT_NAMESPACE = null;

   protected long readLong(String tag, XmlPullParser parser) throws IOException, XmlPullParserException {
      parser.require(XmlPullParser.START_TAG, DEFAULT_NAMESPACE, tag);
      long result = Long.parseLong(read(parser));
      parser.require(XmlPullParser.END_TAG, DEFAULT_NAMESPACE, tag);
      return result;
   }

   protected String readString(String tag, XmlPullParser parser) throws IOException, XmlPullParserException {
      parser.require(XmlPullParser.START_TAG, DEFAULT_NAMESPACE, tag);
      String title = read(parser);
      parser.require(XmlPullParser.END_TAG, DEFAULT_NAMESPACE, tag);
      return title;
   }

   protected String read(XmlPullParser parser) throws IOException, XmlPullParserException {
      String result = "";
      if (parser.next() == XmlPullParser.TEXT) {
         result = parser.getText();
         parser.nextTag();
      }
      return result;
   }

   protected void skip(XmlPullParser parser) throws XmlPullParserException, IOException {
      if (parser.getEventType() != XmlPullParser.START_TAG) {
         throw new IllegalStateException();
      }
      int depth = 1;
      while (depth != 0) {
         switch (parser.next()) {
            case XmlPullParser.END_TAG:
               depth--;
               break;
            case XmlPullParser.START_TAG:
               depth++;
               break;
            default:
               break;
         }
      }
   }

}
