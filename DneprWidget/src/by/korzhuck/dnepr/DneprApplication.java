package by.korzhuck.dnepr;

import android.app.Application;
import android.content.Context;
import com.crashlytics.android.Crashlytics;

public class DneprApplication extends Application {
   @Override
   public void onCreate() {
      super.onCreate();
      Crashlytics.start(this);
      _context = this;
   }

   public static Context context() {
      return _context;
   }

   private static Context _context;
}
