package by.korzhuck.dnepr.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;
import by.korzhuck.dnepr.R;
import by.korzhuck.dnepr.data.Match;

import java.util.List;

public class FixturesAdapter extends ArrayAdapter<Match> {
   public FixturesAdapter(Context context, List<Match> fixtures) {
      super(context, R.layout.list_item_match, fixtures);
   }
}
