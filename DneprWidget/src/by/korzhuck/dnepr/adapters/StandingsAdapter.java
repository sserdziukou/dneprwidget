package by.korzhuck.dnepr.adapters;

import android.content.Context;
import android.widget.ArrayAdapter;
import by.korzhuck.dnepr.R;
import by.korzhuck.dnepr.data.Standing;

import java.util.List;

public class StandingsAdapter extends ArrayAdapter<Standing> {
   public StandingsAdapter(Context context, List<Standing> standings) {
      super(context, R.layout.list_item_standing, standings);
   }
}
